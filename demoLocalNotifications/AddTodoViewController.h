//
//  AddTodoViewController.h
//  demoLocalNotifications
//
//  Created by Thao on 8/26/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddTodoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)btnSaveClicked:(id)sender;

@end
