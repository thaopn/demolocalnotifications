//
//  AppDelegate.h
//  demoLocalNotifications
//
//  Created by Thao on 8/26/14.
//  Copyright (c) 2014 thao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
